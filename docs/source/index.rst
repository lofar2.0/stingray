====================================================
Welcome to the documentation of Stingray
====================================================

..
    To define more variables see rst_epilog generation in conf.py

Documentation for version: |version|

Contents:

.. toctree::
   :maxdepth: 2

   readme
   source_documentation/index
   deployment
