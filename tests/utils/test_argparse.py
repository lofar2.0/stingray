#  Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""argparse utils tests"""
import unittest
from datetime import datetime, timezone

from lofar_stingray.utils.argparse import (
    kv_dict,
    datetime_from_iso_format_with_tz,
    str_lower,
)


class TestArgparseUtils(unittest.TestCase):
    """argparse utils tests"""

    def test_datetime_from_iso_format_with_tz(self):
        """test datetime_from_iso_format_with_tz helper"""
        assert datetime_from_iso_format_with_tz(
            "2024-06-07T11:49:14.000+00:00"
        ) == datetime(2024, 6, 7, 11, 49, 14, tzinfo=timezone.utc)
        assert datetime_from_iso_format_with_tz(
            "2024-06-07T11:49:14.000+02:00"
        ) == datetime(2024, 6, 7, 9, 49, 14, tzinfo=timezone.utc)
        assert datetime_from_iso_format_with_tz("2024-06-07T11:49:14.000") == datetime(
            2024, 6, 7, 11, 49, 14, tzinfo=timezone.utc
        )

    def test_kv_dict(self):
        """test kv_dict parser helper"""
        in_str = "key1=val1,key2=ik1=iv1,key3=val3"
        kv = kv_dict(in_str)
        assert kv == {"key1": "val1", "key2": "ik1=iv1", "key3": "val3"}

    def test_str_lower(self):
        """test str_lower helper"""
        in_str = "AaBbCcDd"
        assert str_lower(in_str) == "aabbccdd"


if __name__ == "__main__":
    unittest.main()
