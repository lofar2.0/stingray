#  Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""numpy JSON encoder tests"""
import json
from datetime import datetime
from unittest import TestCase

import numpy

from lofar_stingray.utils import StingrayJsonEncoder


class TestStingrayJsonEncoder(TestCase):
    """Test cases of the StingrayJsonEncoder class"""

    def test_serialize_np_array_simple(self):
        """Test if a numpy array is correctly serialized"""

        data = numpy.full(2, 10)
        json_data = json.dumps(data, cls=StingrayJsonEncoder)

        deserialized = json.loads(json_data)
        self.assertEqual([10, 10], deserialized)

    def test_serialize_np_array_multi(self):
        """Test if a numpy array is correctly serialized"""

        data = numpy.full((2, 2), 5)
        json_data = json.dumps(data, cls=StingrayJsonEncoder)

        deserialized = json.loads(json_data)
        self.assertEqual([[5, 5], [5, 5]], deserialized)

    def test_date(self):
        """Test if a datetime is correctly serialized"""
        data = {"ts": datetime.fromisoformat("2024-06-02T03:51:41.110706+00:00")}
        json_data = json.dumps(data, cls=StingrayJsonEncoder)

        deserialized = json.loads(json_data)
        self.assertEqual({"ts": "2024-06-02T03:51:41.110706+00:00"}, deserialized)
