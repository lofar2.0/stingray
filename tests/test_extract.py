#  Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""test extract entry point"""
import os
import unittest
from unittest.mock import patch, ANY

from lofar_stingray import extract


class TestExtract(unittest.TestCase):
    """test extract entry point"""

    @patch.dict(
        os.environ,
        {"MINIO_ROOT_USER": "user", "MINIO_ROOT_PASSWORD": "pass"},
        clear=True,
    )
    @patch("lofar_stingray._minio.Minio", autospec=True)
    def test_main(self, m_minio):
        """test entrypoint without optional parameter"""
        args = [
            "--endpoint",
            "localhost:9000",
            "--secure",
            "cs001",
            "hba1",
            "sst",
            "2024-06-07T11:48:59.000+00:00",
            "2024-06-07T11:49:14.000+00:00",
            "s3://central-statistics",
            "s3://hdf-test/prefix/cs001-hba1-sst.h5",
        ]
        ret = extract.main(args)
        assert ret == 0
        m_minio.return_value.fput_object.assert_called_with(
            "hdf-test", "/prefix/cs001-hba1-sst.h5", ANY, metadata=None
        )

    @patch.dict(
        os.environ,
        {"MINIO_ROOT_USER": "user", "MINIO_ROOT_PASSWORD": "pass"},
        clear=True,
    )
    @patch("lofar_stingray._minio.Minio", autospec=True)
    def test_main_user_meta(self, m_minio):
        """test entrypoint with optional user-metadata parameter"""

        args = [
            "--endpoint",
            "localhost:9000",
            "--secure",
            "cs001",
            "hba1",
            "sst",
            "2024-06-07T11:48:59.000+00:00",
            "2024-06-07T11:49:14.000+00:00",
            "s3://central-statistics",
            "s3://hdf-test/prefix/cs001-hba1-sst.h5",
            "--user-metadata",
            "key1=val1,key2=ik1=iv1,key3=val3",
        ]
        ret = extract.main(args)
        assert ret == 0
        m_minio.return_value.fput_object.assert_called_with(
            "hdf-test",
            "/prefix/cs001-hba1-sst.h5",
            ANY,
            metadata={"key1": "val1", "key2": "ik1=iv1", "key3": "val3"},
        )
