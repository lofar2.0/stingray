#  Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""Testing of the forward program"""
import filecmp
from os.path import dirname
from unittest import TestCase
from unittest.mock import patch
from tempfile import TemporaryDirectory

from prometheus_client.registry import REGISTRY

from lofar_stingray import forward


class TestForward(TestCase):
    """Test cases of the forward program"""

    def setUp(self):
        super().setUp()

        # Clear all metrics to make sure we don't register the same one twice
        # between tests (the library forbids it).
        #
        # Unfortunately, Metrics draw copies of prometheus_client.registry.REGISTRY,
        # and that does not provide a public interface to query the registered
        # collectors.

        # pylint: disable=protected-access
        for collector in list(REGISTRY._collector_to_names.keys()):
            REGISTRY.unregister(collector)

    @patch("lofar_stingray.forward.start_http_server", autospec=True)
    @patch("lofar_stingray.streams._create.ZeroMQReceiver", autospec=True)
    def test_json_zmq_to_file(self, m_receiver, _m_prometheus_server):
        """Test forwarding packets from ZMQ to file."""

        m_receiver.return_value.__enter__.return_value.get_json.side_effect = [
            {"a": "foo"},
            {"b": "bar"},
            None,
        ]

        with TemporaryDirectory() as tmpdir:
            forward.main(
                [
                    "zmq+tcp://localhost:6001/topic",
                    f"file:{tmpdir}/output.json",
                    "--datatype=json",
                ]
            )

            with open(f"{tmpdir}/output.json", encoding="utf-8") as f:
                lines = [line.rstrip() for line in f]

        self.assertEqual('{"a": "foo"}', lines[0])
        self.assertEqual('{"b": "bar"}', lines[1])

    @patch("lofar_stingray.forward.start_http_server", autospec=True)
    def test_packet_file_to_file(self, _m_prometheus_server):
        """Test forwarding packets from file to file."""

        with TemporaryDirectory() as tmpdir:

            input_file = f"{dirname(__file__)}/publish/SDP_BST_statistics_packets.bin"
            output_file = f"{tmpdir}/output.bin"
            forward.main(
                [
                    f"file:{input_file}",
                    f"file:{output_file}",
                    "--datatype=packet",
                ]
            )

            # files must be identical after forwarding
            self.assertTrue(filecmp.cmp(input_file, output_file))
