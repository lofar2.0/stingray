#  Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""Test streams/create"""

from unittest import TestCase

from lofar_stingray.streams import (
    create,
    ZeroMQReceiver,
    TCPStream,
    UDPStream,
    FileStream,
)


class TestCreate(TestCase):
    """Test create, the Stream factory function."""

    def test_tcp_listen(self):
        """Test seting up a tcp stream to listen on."""
        sut = create("tcp://localhost:9999")

        self.assertIsInstance(sut, TCPStream)

    def test_udp_listen(self):
        """Test seting up a udp stream to listen on."""
        sut = create("udp://localhost:9999")

        self.assertIsInstance(sut, UDPStream)

    def test_file_read(self):
        """Test seting up a file to read from."""

        # we don't actually open this file so it's
        # ok to construct this
        sut = create("file:///file")

        self.assertIsInstance(sut, FileStream)

    def test_zmq_subscribe(self):
        """Test seting up a ZMQ connection to subscribe to"""
        sut = create("zmq+tcp://localhost:9999/test2")
        self.assertIsInstance(sut, ZeroMQReceiver)

    def test_unknown(self):
        """Test unsupported schemes."""
        with self.assertRaises(ValueError):
            create("example://localhost:8080")
