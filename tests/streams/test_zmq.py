#  Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""Test ZeroMQReceiver"""

from unittest import mock, TestCase

import zmq

from lofar_stingray.streams import ZeroMQReceiver


@mock.patch("zmq.Context.instance")
class TestZeroMQReceiver(TestCase):
    """Test ZeroMQReceiver"""

    def test_text(self, ctx):
        """Test receiving messages encoded as text/plain."""

        socket_mock = mock.Mock()
        ctx.return_value = mock.Mock()
        ctx.return_value.socket.return_value = socket_mock
        socket_mock.recv_multipart.side_effect = [
            ["test".encode(), "2024-05-17T08:35:48".encode(), "data1".encode()],
            ["test".encode(), "2024-05-17T08:36:48".encode(), "data2".encode()],
            zmq.ZMQError(errno=zmq.ETERM),
        ]

        receiver = ZeroMQReceiver(
            "tcp://localhost:9999", ["test"], content_type="text/plain"
        )

        # on start, any subband should map on the first entry
        self.assertListEqual(["test"], receiver.topics)
        self.assertEqual("text/plain", receiver.content_type)

        packages = []
        for package in receiver:
            packages.append(package)

        self.assertListEqual(
            ["data1", "data2"],
            packages,
        )

    def test_json(self, ctx):
        """Test receiving messages encoded as application/json."""

        socket_mock = mock.Mock()
        ctx.return_value = mock.Mock()
        ctx.return_value.socket.return_value = socket_mock
        socket_mock.recv_multipart.side_effect = [
            [
                "test2".encode(),
                "2024-05-18T08:35:48".encode(),
                '{"data": "one"}'.encode(),
            ],
            [
                "test2".encode(),
                "2024-05-18T08:36:48".encode(),
                '{"data": "two"}'.encode(),
            ],
            zmq.ZMQError(errno=zmq.ETERM),
        ]
        receiver = ZeroMQReceiver(
            "tcp://localhost:9999",
            topics=["test2"],
            content_type="application/json",
        )
        self.assertListEqual(["test2"], receiver.topics)
        self.assertEqual("application/json", receiver.content_type)

        packages = []
        for package in receiver:
            packages.append(package)

        self.assertListEqual(
            [
                {"data": "one"},
                {"data": "two"},
            ],
            packages,
        )

    def test_binary(self, ctx):
        """Test receiving messages encoded as application/octet-stream."""

        socket_mock = mock.Mock()
        ctx.return_value = mock.Mock()
        ctx.return_value.socket.return_value = socket_mock
        socket_mock.recv_multipart.side_effect = [
            ["test2".encode(), "2024-05-19T08:35:48".encode(), b"{data1}"],
            ["test2".encode(), "2024-05-19T08:36:48".encode(), b"{data2}"],
            zmq.ZMQError(errno=zmq.ETERM),
        ]
        receiver = ZeroMQReceiver(
            "tcp://localhost:9999/",
            topics=["test2"],
            content_type="application/octet-stream",
        )
        self.assertListEqual(["test2"], receiver.topics)
        self.assertEqual("application/octet-stream", receiver.content_type)

        packages = []
        for package in receiver:
            packages.append(package)

        self.assertListEqual(
            [
                b"{data1}",
                b"{data2}",
            ],
            packages,
        )
