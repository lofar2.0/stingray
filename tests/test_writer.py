#  Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""Testing of the writer module"""
from datetime import datetime, timedelta, timezone
from unittest import TestCase
from unittest.mock import patch, ANY, create_autospec

from minio import Minio

from lofar_stingray.writer import Block, Storage


class TestStorage(TestCase):
    """Test cases of the storage class"""

    def setUp(self):
        self.mock_minio = create_autospec(Minio)

    def test_create_bucket_if_not_exists(self):
        """Test if bucket is created if not exists"""
        self.mock_minio.bucket_exists.return_value = False

        Storage("bucket", "prefix", self.mock_minio)

        self.mock_minio.make_bucket.assert_called_once_with("bucket")

    def test_dont_create_bucket_if_exists(self):
        """Test if bucket is not created if exists"""
        self.mock_minio.bucket_exists.return_value = True

        Storage("bucket", "prefix", self.mock_minio)

        self.assertFalse(self.mock_minio.make_bucket.called)

    def test_set_bucket_lifecycle(self):
        """Test if bucket lifecycle policy is set"""
        Storage("bucket", "prefix", self.mock_minio)

        self.mock_minio.set_bucket_lifecycle.assert_called_once()

    @patch("lofar_stingray.writer.datetime", autospec=True)
    def test_write_line(self, mock_datetime):
        """Test if write line is sending chunked data to storage backend"""
        mock_datetime.now.return_value = datetime(
            year=2023,
            month=11,
            day=24,
            hour=12,
            minute=12,
            second=12,
            tzinfo=timezone.utc,
        )
        storage = Storage("bucket", "prefix", self.mock_minio)

        with storage:
            storage.write_line("test")
            mock_datetime.now.return_value = datetime(
                year=2023,
                month=11,
                day=25,
                hour=12,
                minute=12,
                second=12,
                tzinfo=timezone.utc,
            )
            storage.write_line("test2")
            self.mock_minio.put_object.assert_called_with(
                "bucket",
                "prefix/2023/11/25/2023-11-25T12:12:12+00:00.json",
                ANY,
                len(b"test\n"),
                content_type="application/json",
            )

        self.mock_minio.put_object.assert_called_with(
            "bucket",
            "prefix/2023/11/25/2023-11-25T12:12:12+00:00.json",
            ANY,
            len(b"test2\n"),
            content_type="application/json",
        )

    @patch("lofar_stingray.writer.datetime", autospec=True)
    def test_discard_empty(self, mock_datetime):
        """Test if write line is sending chunked data to storage backend"""
        mock_datetime.now.return_value = datetime(
            year=2023,
            month=11,
            day=24,
            hour=12,
            minute=12,
            second=12,
            tzinfo=timezone.utc,
        )
        storage = Storage("bucket", "prefix", self.mock_minio)

        with storage:
            mock_datetime.now.return_value = datetime(
                year=2023,
                month=11,
                day=25,
                hour=12,
                minute=12,
                second=12,
                tzinfo=timezone.utc,
            )

        self.assertFalse(self.mock_minio.put_object.called)


class TestBlock(TestCase):
    """Test cases of the block class"""

    def test_expired(self):
        """Test if expired is working correctly"""
        block = Block(timedelta(minutes=5))
        block2 = Block(timedelta(seconds=5))

        with patch("lofar_stingray.writer.datetime") as mock_datetime:
            mock_datetime.now.return_value = block.start + timedelta(minutes=1)
            self.assertFalse(block.expired())

            mock_datetime.now.return_value = block.start + timedelta(minutes=5)
            self.assertFalse(block.expired())

            mock_datetime.now.return_value = block.start + timedelta(minutes=6)
            self.assertTrue(block.expired())

            mock_datetime.now.return_value = block2.start + timedelta(seconds=1)
            self.assertFalse(block2.expired())

            mock_datetime.now.return_value = block2.start + timedelta(seconds=6)
            self.assertTrue(block2.expired())
