#  Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""BstAggregator tests"""
import json
from os.path import dirname
from unittest import TestCase

import numpy

from lofar_station_client.dts.constants import N_pol

from lofar_stingray.aggregator import BstAggregator


class TestBstAggregator(TestCase):
    """Test cases of the BstAggregator class"""

    def test_packets_to_matrix(self):
        """Test packets_to_matrix method"""
        packets = []

        # load packets for one timestamp
        with open(dirname(__file__) + "/bst.json", encoding="utf-8") as f:
            for line in f:
                packet = json.loads(line)
                packet["payload"] = numpy.array(packet["payload"], dtype=numpy.float32)
                packets.append(packet)

        sut = BstAggregator()
        matrix = sut.packets_to_matrix(packets)

        numpy.testing.assert_array_equal(
            matrix, numpy.ones((BstAggregator.MAX_BEAMLETS, N_pol), dtype=numpy.float32)
        )

    def test_exceed_beamlets(self):
        """Test packets_to_matrix method"""
        packets = [
            {
                "source_info": {"payload_error": False},
                "data_id": {"beamlet_index": 487},
                "payload": numpy.array([0, 0, 0], dtype=numpy.float32),
            }
        ]
        sut = BstAggregator()

        with self.assertRaises(ValueError):
            _ = sut.packets_to_matrix(packets)
