#  Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""XstAggregator tests"""
import json
from os.path import dirname
from unittest import TestCase

import numpy

from lofar_stingray.aggregator import XstAggregator


class TestXstAggregator(TestCase):
    """Test cases of the XstAggregator class"""

    def test_packets_to_matrix(self):
        """Test packets_to_matrix method"""
        packets = []

        # load packets for one timestamp
        with open(dirname(__file__) + "/xst.json", encoding="utf-8") as f:
            for line in f:
                packet = json.loads(line)
                packet["payload"] = numpy.array(packet["payload"], dtype=numpy.float32)
                packets.append(packet)

        sut = XstAggregator()
        matrix = sut.packets_to_matrix(packets)

        self.assertEqual(102, matrix[0][0])

        numpy.testing.assert_array_equal(
            matrix[0][1][0][0:3],
            numpy.array(
                [
                    1.2392113e10 + 0.000000e00j,
                    1.4179863e10 + 1.883808e10j,
                    2.4097221e10 + 6.114040e09j,
                ],
                dtype=numpy.complex64,
            ),
        )

    def test_convert_exceed_input_index(self):
        """Test convert method"""
        packets = [
            {
                "source_info": {"payload_error": False},
                "data_id": {"first_baseline": 0},
                "nof_signal_inputs": 99,
                "payload": numpy.array([0, 0, 0], dtype=numpy.float32),
            }
        ]
        sut = XstAggregator(packets, first_signal_input_index=0)

        with self.assertRaises(ValueError):
            _ = sut.packets_to_matrix(packets)

        packets = [
            {
                "source_info": {"payload_error": False},
                "data_id": {"first_baseline": [0, 0]},
                "nof_signal_inputs": 12,
                "payload": numpy.array([0, 0, 0], dtype=numpy.float32),
            }
        ]
        sut = XstAggregator(
            first_signal_input_index=99,
            nr_signal_inputs=0,
        )

        with self.assertRaises(ValueError):
            _ = sut.packets_to_matrix(packets)

        packets = [
            {
                "source_info": {"payload_error": False},
                "data_id": {"first_baseline": [1, 0]},
                "nof_signal_inputs": 12,
                "payload": numpy.array([0, 0, 0], dtype=numpy.float32),
            }
        ]
        sut = XstAggregator(
            first_signal_input_index=0,
            nr_signal_inputs=100,
        )

        with self.assertRaises(ValueError):
            _ = sut.packets_to_matrix(packets)
