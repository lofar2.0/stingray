#  Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""CollectPacketsPerTimestamp tests"""
from datetime import datetime
from unittest import TestCase

from lofar_stingray.aggregator import CollectPacketsPerTimestamp

# pylint: disable=too-few-public-methods


class TestCollectPacketsPerTimestamp(TestCase):
    """Test cases of the CollectPacketsPerTimestamp class"""

    class FakeStatisticsPacket:
        """A mock for a StatisticsPacket"""

        def __init__(self, timestamp):
            self.timestamp = timestamp

        def __repr__(self):
            return f"FakeStatisticsPacket({self.timestamp})"

    def test_grouping(self):
        """Test whether packets get grouped per timestamp."""

        timestamps = [
            datetime(2024, 11, 1),
            datetime(2024, 11, 1),
            datetime(2024, 11, 1),
            datetime(2024, 11, 2),
            datetime(2024, 11, 2),
            datetime(2024, 11, 3),
        ]

        packets = [self.FakeStatisticsPacket(ts) for ts in timestamps]

        sut = CollectPacketsPerTimestamp()
        groups = [
            group for packet in packets for group in sut.put_packet(packet)
        ] + list(sut.done())

        self.assertEqual(3, len(groups))
        self.assertEqual({datetime(2024, 11, 1)}, {p.timestamp for p in groups[0]})
        self.assertEqual({datetime(2024, 11, 2)}, {p.timestamp for p in groups[1]})
        self.assertEqual({datetime(2024, 11, 3)}, {p.timestamp for p in groups[2]})
