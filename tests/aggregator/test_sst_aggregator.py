#  Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""SstAggregator tests"""
import json
from os.path import dirname
from unittest import TestCase

import numpy

from lofar_stingray.aggregator import SstAggregator


class TestSstAggregator(TestCase):
    """Test cases of the SstAggregator class"""

    def test_packets_to_matrix(self):
        """Test packets_to_matrix method"""
        packets = []

        # load packets for one timestamp
        with open(dirname(__file__) + "/sst.json", encoding="utf-8") as f:
            for line in f:
                packet = json.loads(line)
                packet["payload"] = numpy.array(packet["payload"], dtype=numpy.float32)
                packets.append(packet)

        sut = SstAggregator()
        matrix = sut.packets_to_matrix(packets)

        numpy.testing.assert_array_equal(
            matrix[120][:5],
            numpy.array([118174500342, 4248876, 679874756, 2671774, 2472221]).astype(
                numpy.float32
            ),
        )
        numpy.testing.assert_array_equal(
            matrix[120][507:512],
            numpy.array([5534594, 5535017, 5519003, 128035163225, 5514898]).astype(
                numpy.float32
            ),
        )

    def test_exceed_input_index(self):
        """Test packets_to_matrix method"""
        packets = [
            {
                "source_info": {"payload_error": False},
                "data_id": {"signal_input_index": 0},
                "payload": numpy.array([0, 0, 0], dtype=numpy.float32),
            }
        ]
        sut = SstAggregator(first_signal_input_index=1)

        with self.assertRaises(ValueError):
            _ = sut.packets_to_matrix(packets)

        sut = SstAggregator(nr_signal_inputs=0)

        with self.assertRaises(ValueError):
            _ = sut.packets_to_matrix(packets)
