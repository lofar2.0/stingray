#  Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""Testing of the publish program"""
from datetime import timezone
import json
import numpy
from os.path import dirname
from unittest import TestCase
from unittest.mock import patch

from lofar_stingray import publish
from lofar_stingray.streams import FileStream

from prometheus_client.registry import REGISTRY


class TestPublish(TestCase):
    """Test cases of the publish program"""

    def setUp(self):
        super().setUp()

        # Clear all metrics to make sure we don't register the same one twice
        # between tests (the library forbids it).
        #
        # Unfortunately, Metrics draw copies of prometheus_client.registry.REGISTRY,
        # and that does not provide a public interface to query the registered
        # collectors.
        for collector in list(REGISTRY._collector_to_names.keys()):
            REGISTRY.unregister(collector)

    def test_packets_to_messages(self):
        """Test messages created by packets_to_messages."""

        with FileStream(f"{dirname(__file__)}/SDP_BST_statistics_packets.bin") as f:
            bst_packets = [packet for packet in f]

        messages = publish.packets_to_messages("cs001", "lba", "bst", bst_packets)

        self.assertTrue(len(messages) > 0)
        self.assertEqual(timezone.utc, messages[0].timestamp.tzinfo)
        self.assertEqual(5, messages[0].packet_version)

        # verify BST data dimensionality
        self.assertEqual(488, len(messages[0].bst_data))
        self.assertEqual(2, len(messages[0].bst_data[0]))

        # verify station info
        self.assertEqual("cs001", messages[0].station)
        self.assertEqual("lba", messages[0].antenna_field)
        self.assertEqual("bst", messages[0].type)
        self.assertEqual(dict, type(messages[0].station_info))
        self.assertEqual(903, messages[0].station_info["station_id"])
        self.assertEqual(0, messages[0].station_info["antenna_field_index"])

        # verify frequency information
        self.assertEqual(200, messages[0].f_adc)
        self.assertEqual(dict, type(messages[0].source_info))
        self.assertEqual(0, messages[0].source_info["antenna_band_index"])
        self.assertEqual(0, messages[0].source_info["nyquist_zone_index"])
        self.assertEqual(1, messages[0].source_info["t_adc"])

    @patch("lofar_stingray.publish.start_http_server", autospec=True)
    @patch("lofar_stingray.publish.ZeroMQPublisher", autospec=True)
    def test_bst(self, m_publisher, m_prometheus_server):
        publish.main(
            [
                "cs001",
                "lba",
                "bst",
                f"file:{dirname(__file__)}/SDP_BST_statistics_packets.bin",
            ]
        )
        published_messages = [
            call.args[0]
            for call in m_publisher.return_value.__enter__.return_value.send.mock_calls
        ]

        # file contains four timestamps, so there should be four messages
        self.assertEqual(4, len(published_messages))

        # messages should be valid json
        bst_messages = [json.loads(msg) for msg in published_messages]

        # check fields
        for idx, bst_message in enumerate(bst_messages):
            self.assertEqual("cs001", bst_message["station"], f"{idx=}, {bst_message=}")
            self.assertEqual(
                "lba", bst_message["antenna_field"], f"{idx=}, {bst_message=}"
            )
            self.assertEqual("bst", bst_message["type"], f"{idx=}, {bst_message=}")
            self.assertEqual(
                (488, 2),
                numpy.array(bst_message["bst_data"]).shape,
                f"{idx=}, {bst_message=}",
            )

        # check timestamps
        self.assertEqual(
            "2022-05-20T11:08:44.999997+00:00", bst_messages[0]["timestamp"]
        )
        self.assertEqual("2022-05-20T11:08:46+00:00", bst_messages[1]["timestamp"])
        self.assertEqual(
            "2022-05-20T11:08:46.999997+00:00", bst_messages[2]["timestamp"]
        )
        self.assertEqual("2022-05-20T11:08:48+00:00", bst_messages[3]["timestamp"])

    @patch("lofar_stingray.publish.start_http_server", autospec=True)
    @patch("lofar_stingray.publish.ZeroMQPublisher", autospec=True)
    def test_sst(self, m_publisher, m_prometheus_server):
        publish.main(
            [
                "cs001",
                "lba",
                "sst",
                f"file:{dirname(__file__)}/SDP_SST_statistics_packets.bin",
            ]
        )
        published_messages = [
            call.args[0]
            for call in m_publisher.return_value.__enter__.return_value.send.mock_calls
        ]

        # file contains 12 timestamps, so there should be 12 messages
        self.assertEqual(12, len(published_messages))

        # messages should be valid json
        sst_messages = [json.loads(msg) for msg in published_messages]

        # check fields
        for idx, sst_message in enumerate(sst_messages):
            self.assertEqual("cs001", sst_message["station"], f"{idx=}, {sst_message=}")
            self.assertEqual(
                "lba", sst_message["antenna_field"], f"{idx=}, {sst_message=}"
            )
            self.assertEqual("sst", sst_message["type"], f"{idx=}, {sst_message=}")
            self.assertEqual(
                (192, 512),
                numpy.array(sst_message["sst_data"]).shape,
                f"{idx=}, {sst_message=}",
            )

        # check timestamps
        self.assertEqual("2021-09-20T12:17:40+00:00", sst_messages[0]["timestamp"])
        self.assertEqual(
            "2021-09-20T12:17:40.999997+00:00", sst_messages[1]["timestamp"]
        )
        self.assertEqual("2021-09-20T12:17:42+00:00", sst_messages[2]["timestamp"])
        self.assertEqual(
            "2021-09-20T12:17:42.999997+00:00", sst_messages[3]["timestamp"]
        )
        self.assertEqual("2021-09-20T12:17:44+00:00", sst_messages[4]["timestamp"])
        self.assertEqual(
            "2021-09-20T12:17:44.999997+00:00", sst_messages[5]["timestamp"]
        )
        self.assertEqual("2021-09-20T12:17:46+00:00", sst_messages[6]["timestamp"])
        self.assertEqual(
            "2021-09-20T12:17:46.999997+00:00", sst_messages[7]["timestamp"]
        )
        self.assertEqual("2021-09-20T12:17:48+00:00", sst_messages[8]["timestamp"])
        self.assertEqual(
            "2021-09-20T12:17:48.999997+00:00", sst_messages[9]["timestamp"]
        )
        self.assertEqual("2021-09-20T12:17:50+00:00", sst_messages[10]["timestamp"])
        self.assertEqual(
            "2021-09-20T12:17:50.999997+00:00", sst_messages[11]["timestamp"]
        )

    @patch("lofar_stingray.publish.start_http_server", autospec=True)
    @patch("lofar_stingray.publish.ZeroMQPublisher", autospec=True)
    def test_xst(self, m_publisher, m_prometheus_server):
        publish.main(
            [
                "cs001",
                "lba",
                "xst",
                f"file:{dirname(__file__)}/SDP_XST_statistics_packets.bin",
            ]
        )
        published_messages = [
            call.args[0]
            for call in m_publisher.return_value.__enter__.return_value.send.mock_calls
        ]

        # file contains 7 timestamps, so there should be 7 messages
        self.assertEqual(7, len(published_messages))

        # messages should be valid json
        xst_messages = [json.loads(msg) for msg in published_messages]

        # check fields
        for idx, xst_message in enumerate(xst_messages):
            self.assertEqual("cs001", xst_message["station"], f"{idx=}, {xst_message=}")
            self.assertEqual(
                "lba", xst_message["antenna_field"], f"{idx=}, {xst_message=}"
            )
            self.assertEqual("xst", xst_message["type"], f"{idx=}, {xst_message=}")
            self.assertEqual(102, xst_message["subband"], f"{idx=}, {xst_message=}")
            self.assertEqual(
                (192, 192),
                numpy.array(xst_message["xst_data_real"]).shape,
                f"{idx=}, {xst_message=}",
            )
            self.assertEqual(
                (192, 192),
                numpy.array(xst_message["xst_data_imag"]).shape,
                f"{idx=}, {xst_message=}",
            )

        # check timestamps
        self.assertEqual("2021-09-13T13:21:32+00:00", xst_messages[0]["timestamp"])
        self.assertEqual(
            "2021-09-13T13:21:32.999997+00:00", xst_messages[1]["timestamp"]
        )
        self.assertEqual("2021-09-13T13:21:34+00:00", xst_messages[2]["timestamp"])
        self.assertEqual(
            "2021-09-13T13:21:34.999997+00:00", xst_messages[3]["timestamp"]
        )
        self.assertEqual("2021-09-13T13:21:36+00:00", xst_messages[4]["timestamp"])
        self.assertEqual(
            "2021-09-13T13:21:36.999997+00:00", xst_messages[5]["timestamp"]
        )
        self.assertEqual("2021-09-13T13:21:38+00:00", xst_messages[6]["timestamp"])
