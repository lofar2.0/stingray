#  Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""S3PacketLoader tests"""
from datetime import datetime, timedelta
from unittest import TestCase
from unittest.mock import create_autospec, Mock, MagicMock, patch

from minio import Minio

import lofar_stingray.reader._s3_packet_loader
from lofar_stingray.reader import S3PacketLoader

# pylint: disable=protected-access


class TestS3PacketLoader(TestCase):
    """Test cases of the S3PacketLoader class"""

    TEST_JSON_DATA = {
        "file1": '{"ts": "2024-06-07T13:20:32+00:00", "info": "ignored_packet"}\n'
        '{"ts": "2024-06-07T13:21:33+00:00", "info": "1"}',
        "file2": '{"ts": "2024-06-07T13:21:34+00:00", "info": "2"}\n'
        '{"ts": "2024-06-07T13:22:35+00:00", "info": "3"}',
        "file3": '{"ts": "2024-06-07T13:23:36+00:00", "info": "4"}\n'
        '{"ts": "2024-06-07T14:25:33+00:00", "info": "ignored_packet"}',
    }

    def _mock_response(self, data: str):
        """Create a magic mock for the urllib response"""
        response = MagicMock()
        response.__iter__.return_value = data.split(sep="\n")
        return response

    def setUp(self):
        self.mock_minio = create_autospec(Minio)

        # speed up polling
        lofar_stingray.reader._s3_packet_loader.MINIO_POLL_INTERVAL = timedelta(
            microseconds=1000
        )
        lofar_stingray.reader._s3_packet_loader.MAX_BLOCK_SYNC_DELAY = timedelta(
            seconds=5
        )

    def test_wait_for_filenames_after_already_there(self):
        """Test wait_for_filenames_after for data from the distant past."""
        timestamp = datetime.fromisoformat("2024-06-07T13:21:32+00:00")

        self.mock_minio.list_objects.return_value = iter(
            [
                Mock(object_name="file1"),
                Mock(object_name="file2"),
                Mock(object_name="file3"),
            ]
        )

        packet_loader = S3PacketLoader("bucket", "prefix/", self.mock_minio)

        result = packet_loader.wait_for_filenames_after(timestamp)
        self.assertTrue(result)  # objects were returned

        self.mock_minio.list_objects.assert_called_with(
            "bucket",
            recursive=True,
            prefix="prefix",
            start_after="prefix/2024/06/07/2024-06-07T13:21:32+00:00.json",
        )

    def test_wait_for_filenames_after_never_arrive(self):
        """Test wait_for_filenames_after for future packets that do not arrive."""
        timestamp = datetime.fromisoformat("2024-06-07T13:21:32+00:00")

        packet_loader = S3PacketLoader(
            "bucket", "prefix/", self.mock_minio, block_duration=timedelta(seconds=10)
        )

        with patch("lofar_stingray.reader._s3_packet_loader.datetime") as mock_datetime:
            mock_datetime.now.side_effect = [
                datetime.fromisoformat(f"2024-06-07T13:21:{seconds}+00:00")
                for seconds in range(32, 50)
            ]
            self.mock_minio.list_objects.side_effect = [iter([]) for n in range(50)]

            result = packet_loader.wait_for_filenames_after(timestamp)
            self.assertFalse(result)  # no objects were returned

    def test_wait_for_filenames_after_arrive_during_wait(self):
        """Test wait_for_filenames_after for future packets that
        arrive while waiting."""
        timestamp = datetime.fromisoformat("2024-06-07T13:21:32+00:00")

        packet_loader = S3PacketLoader(
            "bucket", "prefix/", self.mock_minio, block_duration=timedelta(seconds=10)
        )

        with patch("lofar_stingray.reader._s3_packet_loader.datetime") as mock_datetime:
            mock_datetime.now.side_effect = [
                datetime.fromisoformat(f"2024-06-07T13:21:{seconds}+00:00")
                for seconds in range(32, 50)
            ]
            self.mock_minio.list_objects.side_effect = [iter([]) for n in range(5)] + [
                iter([Mock(object_name="file3")]),
            ]
            result = packet_loader.wait_for_filenames_after(timestamp)
            self.assertTrue(result)  # objects were returned

    def test_load_json(self):
        """Test if a numpy array is correctly serialized"""
        start = datetime.fromisoformat("2024-06-07T13:21:32+00:00")
        end = datetime.fromisoformat("2024-06-07T13:25:32+00:00")

        self.mock_minio.list_objects.return_value = iter(
            [
                Mock(object_name="file1"),
                Mock(object_name="file2"),
                Mock(object_name="file3"),
            ]
        )
        self.mock_minio.get_object.side_effect = lambda _, file: self._mock_response(
            self.TEST_JSON_DATA[file]
        )

        packet_loader = S3PacketLoader("bucket", "prefix/", self.mock_minio)

        returned_data = False
        for packet in packet_loader.load_json(start, end, ts_field="ts"):
            returned_data = True
            self.assertNotEqual("ignored_packet", packet["info"])
            self.assertIn(packet["info"], [str(x) for x in range(0, 5)])

        self.assertTrue(returned_data)
        self.mock_minio.list_objects.assert_called_with(
            "bucket",
            recursive=True,
            prefix="prefix",
            start_after="prefix/2024/06/07/2024-06-07T13:21:32+00:00.json",
        )
