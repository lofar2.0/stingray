#  Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""XstAnnotator tests"""
from unittest import TestCase

import numpy
from lofar_station_client.statistics.statistics_data import StatisticsDataFile

from lofar_stingray.annotator import XstAnnotator


class TestXstAnnotator(TestCase):
    """Test cases of the XstAnnotator class"""

    def test_mode(self):
        """Test mode property"""
        metadata_packets = []
        matrices = []
        sut = XstAnnotator("test", "hba", metadata_packets, matrices)
        self.assertEqual("XST", sut.mode)

    def test_write(self):
        """Test write method"""
        metadata_packets = []
        matrices = [
            {
                "timestamp": "2022-05-20T11:08:45",
                "subband": 1,
                "xst_data_real": numpy.ones((488, 2)),
                "xst_data_imag": numpy.ones((488, 2)),
            },
            {
                "timestamp": "2022-05-20T11:08:45.999997",
                "subband": 2,
                "xst_data_real": numpy.ones((96, 96)),
                "xst_data_imag": numpy.ones((96, 96)),
            },
        ]
        sut = XstAnnotator("test-001", "hba0", metadata_packets, matrices)

        statistics = StatisticsDataFile()
        sut.write(statistics)

        self.assertListEqual(
            list(statistics.keys()),
            [
                "XST_2022-05-20T11:08:45.000_SB001",
                "XST_2022-05-20T11:08:46.000_SB002",
            ],
        )
