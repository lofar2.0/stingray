#  Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""BaseAnnotator tests"""
import json
from datetime import datetime
from os.path import dirname
from unittest import TestCase

from lofar_station_client.statistics.statistics_data import StatisticsDataFile

from lofar_stingray.annotator._base import BaseAnnotator


class _TestAnnotator(BaseAnnotator):

    @property
    def mode(self):
        return "TEST"

    def write(self, statistics: StatisticsDataFile):
        """Not used in unit tests"""

        raise NotImplementedError


class TestBaseAnnotator(TestCase):
    """Test cases of the BaseAnnotator class"""

    def test_format_timestamp(self):
        """Test format_timestamp method"""
        ts = datetime.fromisoformat("2024-06-07T13:21:32+00:00")
        formatted = BaseAnnotator.format_timestamp(ts)
        assert formatted == "2024-06-07T13:21:32.000"

    def test_round_datetime_ms(self):
        """Test round_datetime_ms method"""
        ts = datetime.fromisoformat("2024-06-05 13:21:32.000003+00:00")
        rounded = BaseAnnotator.round_datetime_ms(ts)
        assert rounded == datetime.fromisoformat("2024-06-05T13:21:32+00:00")

        ts = datetime.fromisoformat("2021-09-13 13:21:32.999997+00:00")
        rounded = BaseAnnotator.round_datetime_ms(ts)
        assert rounded == datetime.fromisoformat("2021-09-13 13:21:33+00:00")

    def test_mode(self):
        """Test mode property"""
        metadata_packets = []
        sut = _TestAnnotator("test", "hba", metadata_packets)
        assert sut.mode == "TEST"

    def test_set_file_header(self):
        """Test set_file_header method"""
        metadata_packets = []
        with open(dirname(__file__) + "/metadata.json", encoding="utf-8") as f:
            for line in f:
                metadata_packets.append(json.loads(line))
        sut = _TestAnnotator("test-001", "hba0", metadata_packets)

        statistics = StatisticsDataFile()
        sut.set_file_header(statistics)

        assert statistics.station_name == "test-001"
        assert statistics.mode == "TEST"
        assert statistics.antennafield_device == "stat/afh/hba0"
        assert statistics.antenna_names == [f"H{n}" for n in range(96)]
        assert statistics.antenna_type == "HBA"
        assert statistics.rcu_pcb_id == [
            [9226931, 9204853],
            [9220264, 9224376],
            [9204055, 9190751],
            [9204829, 9216720],
            [9196514, 8520998],
            [9194166, 9204934],
            [9231217, 9197766],
            [9212584, 9230594],
            [9236083, 9191174],
            [9231287, 9196982],
            [9196383, 9201869],
            [9200706, 9219680],
            [9224813, 9194738],
            [9208598, 9227000],
            [9220403, 9197832],
            [9236556, 9212642],
            [9226931, 9204853],
            [9220264, 9224376],
            [9204055, 9190751],
            [9204829, 9216720],
            [9196514, 8520998],
            [9194166, 9204934],
            [9231217, 9197766],
            [9212584, 9230594],
        ]
        assert statistics.rcu_pcb_version == [["R", "R"] for _ in range(24)]
        assert statistics.antenna_status == []
        assert statistics.antenna_usage_mask == [
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            False,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            False,
            False,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
            True,
        ]
        assert statistics.antenna_reference_itrf == [
            [3826885.718981687, 460981.16159554047, 5064665.977510916],
            [3826886.813981269, 460986.03259563074, 5064664.715510937],
            [3826888.5989824426, 460974.6605958278, 5064664.4035111675],
            [3826889.694982025, 460979.5315959182, 5064663.140511189],
            [3826890.790981607, 460984.40159600857, 5064661.877511209],
            [3826891.8879811894, 460989.272596099, 5064660.614511229],
            [3826891.4789831988, 460968.1575961151, 5064662.829511419],
            [3826892.5759827807, 460973.0295962056, 5064661.56551144],
            [3826893.670982363, 460977.9005962959, 5064660.30351146],
            [3826894.767981945, 460982.7715963864, 5064659.039511481],
            [3826895.8639815273, 460987.6415964767, 5064657.7765115015],
            [3826896.959981109, 460992.51259656716, 5064656.513511522],
            [3826895.4559835372, 460966.5275964929, 5064659.991511692],
            [3826896.550983119, 460971.3985965832, 5064658.728511712],
            [3826897.647982701, 460976.2695966737, 5064657.465511733],
            [3826898.743982283, 460981.1405967641, 5064656.202511753],
            [3826899.839981865, 460986.0115968545, 5064654.939511773],
            [3826900.9359814473, 460990.88259694487, 5064653.676511793],
            [3826900.527983457, 460969.76759696106, 5064655.891511984],
            [3826901.6249830388, 460974.63959705154, 5064654.628512004],
            [3826902.7199826213, 460979.5095971418, 5064653.365512025],
            [3826903.8169822036, 460984.3805972323, 5064652.102512045],
            [3826905.600983377, 460973.00859742926, 5064651.790512276],
            [3826906.6969829593, 460977.87959751964, 5064650.527512297],
        ]
        assert statistics.fpga_firmware_version == [
            "2024-03-02T18.27.19_d601da896_lofar2_unb2c_sdp_station_full",
            "2024-03-02T18.27.19_d601da896_lofar2_unb2c_sdp_station_full",
            "2024-03-02T18.27.19_d601da896_lofar2_unb2c_sdp_station_full",
            "2024-03-02T18.27.19_d601da896_lofar2_unb2c_sdp_station_full",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
        ]
        assert statistics.fpga_hardware_version == [
            "UniBoard2c",
            "UniBoard2c",
            "UniBoard2c",
            "UniBoard2c",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
        ]
