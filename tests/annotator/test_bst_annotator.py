#  Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""BstAnnotator tests"""
from unittest import TestCase

import numpy

from lofar_station_client.statistics.statistics_data import StatisticsDataFile

from lofar_stingray.annotator import BstAnnotator


class TestBstAnnotator(TestCase):
    """Test cases of the BstAnnotator class"""

    def test_mode(self):
        """Test mode property"""
        metadata_packets = []
        matrices = []
        sut = BstAnnotator("test", "hba", metadata_packets, matrices)
        self.assertEqual("BST", sut.mode)

    def test_write(self):
        """Test write method"""
        metadata_packets = []
        matrices = [
            {
                "timestamp": "2022-05-20T11:08:45",
                "bst_data": numpy.ones((488, 2)),
            },
            {
                "timestamp": "2022-05-20T11:08:45.999997",
                "bst_data": numpy.ones((488, 2)),
            },
        ]
        sut = BstAnnotator("test-001", "hba0", metadata_packets, matrices)

        statistics = StatisticsDataFile()
        sut.write(statistics)

        self.assertListEqual(
            list(statistics.keys()),
            [
                "BST_2022-05-20T11:08:45.000",
                "BST_2022-05-20T11:08:46.000",
            ],
        )
