#  Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""SstAnnotator tests"""
from unittest import TestCase

import numpy
from lofar_station_client.statistics.statistics_data import StatisticsDataFile

from lofar_stingray.annotator import SstAnnotator


class TestSstAnnotator(TestCase):
    """Test cases of the SstAnnotator class"""

    def test_mode(self):
        """Test mode property"""
        metadata_packets = []
        matrices = []
        sut = SstAnnotator("test", "hba", metadata_packets, matrices)
        self.assertEqual("SST", sut.mode)

    def test_write(self):
        """Test write method"""
        metadata_packets = []
        matrices = [
            {
                "timestamp": "2022-05-20T11:08:45",
                "sst_data": numpy.ones((512, 2)),
            },
            {
                "timestamp": "2022-05-20T11:08:45.999997",
                "sst_data": numpy.ones((512, 2)),
            },
        ]
        sut = SstAnnotator("test-001", "hba0", metadata_packets, matrices)

        statistics = StatisticsDataFile()
        sut.write(statistics)

        self.assertListEqual(
            list(statistics.keys()),
            [
                "SST_2022-05-20T11:08:45.000",
                "SST_2022-05-20T11:08:46.000",
            ],
        )
