#  Copyright (C) 2024 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""Util classes"""

from ._stringray_json_encoder import StingrayJsonEncoder

__all__ = ["StingrayJsonEncoder"]
