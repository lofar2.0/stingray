#  Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
#  SPDX-License-Identifier: Apache-2.0

"""Stingray"""

from importlib import metadata

__version__ = metadata.version("lofar_stingray")
